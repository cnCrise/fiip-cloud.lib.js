# fiip-cloud

A library of fiip on cloud dev.

## Installation

```bash
npm install fiip-cloud --save
```

## Usage

```js
const cloud = require('fiip-cloud');
cloud.init(id, key);
cloud.login();
cloud.setActualVar(id, key, kVal, kLen, vVal, vLen);
cloud.getActualVar(id, key, kVal, kLen);
cloud.vars.onTargetChange(id, kVal, kLen, fun);
cloud.vars.getActual(id, kVal, kLen);
```

## License

MIT

## Reference

[git](https://gitee.com/cnCrise/fiip-cloud.lib.js.git)
