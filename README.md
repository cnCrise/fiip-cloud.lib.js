# fiip-cloud

fiip云端开发函数库.

## 安装

```bash
npm install fiip-cloud --save
```

## 使用

```js
const cloud = require('fiip-cloud');
cloud.init(id, key);
cloud.login();
cloud.setActualVar(id, key, kVal, kLen, vVal, vLen);
cloud.getActualVar(id, key, kVal, kLen);
cloud.vars.onTargetChange(id, kVal, kLen, fun);
cloud.vars.getActual(id, kVal, kLen);
```

## 许可证

MIT

## 相关链接

[git](https://gitee.com/cnCrise/fiip-cloud.lib.js.git)
