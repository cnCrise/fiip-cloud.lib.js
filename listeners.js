'use strict';
const fiip = require('fiip');
let cloud;

const listener0xFFFF = async (msg) => {
  if (fiip.checking.isDuplicate(msg)) {
    msg.cmd = Buffer.from('0000', 'hex'); //取消后续命令
    return;
  }

  const err = msg.errno[0];
  if (err !== 0x00) {
    if (err === 0x11 || err === 0x13 || err === 0x14) {
      cloud.login();
    } else {
      cloud.emit(cloud.event.error, err);
    }
    msg.cmd = Buffer.from('0000', 'hex'); //取消后续命令
  }
};

const listener0xF011 = async (msg) => {
  const id = Buffer.from(msg.body.slice(0, 8));
  const key = Buffer.from(msg.body.slice(8, 12));
  cloud.emit(cloud.event.getId, id, key);
};
const listener0xF012 = async (msg) => {
  cloud.emit(cloud.event.login);
};
const listener0xF013 = async (msg) => {
  cloud.emit(cloud.event.heart);
};
const listener0xF014 = async (msg) => {
  const id = Buffer.from(msg.body.slice(0, 8));
  const key = Buffer.from(msg.body.slice(8, 12));
  cloud.emit(cloud.event.bind, id, key);
};
const listener0xF015 = async (msg) => {
  cloud.emit(cloud.event.unbind);
};
const listener0xF016 = async (msg) => {
  const key = Buffer.from(msg.body.slice(0, 4));
  cloud.emit(cloud.event.enterBinding, key);
};
const listener0x7811 = async (msg) => {
  const errno = 0x00;
  const body = Buffer.from([]);
  const id = Buffer.from(msg.body.slice(0, 8));
  const key = Buffer.from(msg.body.slice(8, 12));
  cloud.emit(cloud.event.beBound, id, key);
  fiip.respond(cloud.txd, msg, errno, body);
};
const listener0x7812 = async (msg) => {
  const errno = 0x00;
  const body = Buffer.from([]);
  const id = Buffer.from(msg.body.slice(0, 8));
  cloud.emit(cloud.event.beUnbound, id);
  fiip.respond(cloud.txd, msg, errno, body);
};

const listener0xF021 = async (msg) => {
  const id = Buffer.from(msg.body.slice(0, 8));
  const kLen = msg.body[8];
  const kVal = Buffer.from(msg.body.slice(9, 9 + kLen));
  const vLen = msg.body[9 + kLen];
  const vVal = Buffer.from(msg.body.slice(10 + kLen, 10 + kLen + vLen));
  cloud.vars.setActual(id, kVal, kLen, vVal, vLen);
};
const listener0xF022 = async (msg) => {
  const id = Buffer.from(msg.body.slice(0, 8));
  const kLen = msg.body[8];
  const kVal = Buffer.from(msg.body.slice(9, 9 + kLen));
  const vLen = msg.body[9 + kLen];
  const vVal = Buffer.from(msg.body.slice(10 + kLen, 10 + kLen + vLen));
  cloud.vars.setActual(id, kVal, kLen, vVal, vLen);
};
const listener0xF023 = async (msg) => {
  const id = Buffer.from(msg.body.slice(0, 8));
  const kLen = msg.body[8];
  const kVal = Buffer.from(msg.body.slice(9, 9 + kLen));
  cloud.emit(cloud.event.subActualVar, id, kVal, kLen);
};
const listener0xF024 = async (msg) => {
  const id = Buffer.from(msg.body.slice(0, 8));
  let i = 8;

  while (i < ((msg.len[0] << 8) | msg.len[1])) {
    cloud.vars.setActual(id, msg.body.slice(i + 1, i + 1 + msg.body[i]), msg.body[i], null, 0);
    i = i + 1 + msg.body[i];
  }
  cloud.emit(cloud.event.getActualAllVar, id);
};
const listener0xF025 = async (msg) => {
  const id = Buffer.from(msg.body.slice(0, 8));
  cloud.emit(cloud.event.subActualAllVar, id);
};
const listener0xF029 = async (msg) => {
  const id = Buffer.from(msg.body.slice(0, 8));
  const kLen = msg.body[8];
  const kVal = Buffer.from(msg.body.slice(9, 9 + kLen));
  const vLen = msg.body[9 + kLen];
  const vVal = Buffer.from(msg.body.slice(10 + kLen, 10 + kLen + vLen));
  cloud.vars.setTarget(id, kVal, kLen, vVal, vLen);
};
const listener0xF02A = async (msg) => {
  const id = Buffer.from(msg.body.slice(0, 8));
  const kLen = msg.body[8];
  const kVal = Buffer.from(msg.body.slice(9, 9 + kLen));
  const vLen = msg.body[9 + kLen];
  const vVal = Buffer.from(msg.body.slice(10 + kLen, 10 + kLen + vLen));
  cloud.vars.setTarget(id, kVal, kLen, vVal, vLen);
};
const listener0xF02B = async (msg) => {
  const id = Buffer.from(msg.body.slice(0, 8));
  const kLen = msg.body[8];
  const kVal = Buffer.from(msg.body.slice(9, 9 + kLen));
  cloud.emit(cloud.event.subTargetVar, id, kVal, kLen);
};
const listener0xF02C = async (msg) => {
  const id = Buffer.from(msg.body.slice(0, 8));
  let i = 8;

  while (i < ((msg.len[0] << 8) | msg.len[1])) {
    cloud.vars.setTarget(id, msg.body.slice(i + 1, i + 1 + msg.body[i]), msg.body[i], null, 0);
    i = i + 1 + msg.body[i];
  }
  cloud.emit(cloud.event.getTargetAllVar, id);
};
const listener0xF02D = async (msg) => {
  const id = Buffer.from(msg.body.slice(0, 8));
  cloud.emit(cloud.event.subTargetAllVar, id);
};
const listener0x7821 = async (msg) => {
  const errno = 0x00;
  const body = Buffer.from([]);
  const id = Buffer.from(msg.body.slice(0, 8));
  const kLen = msg.body[8];
  const kVal = Buffer.from(msg.body.slice(9, 9 + kLen));
  const vLen = msg.body[9 + kLen];
  const vVal = Buffer.from(msg.body.slice(10 + kLen, 10 + kLen + vLen));
  cloud.vars.setActual(id, kVal, kLen, vVal, vLen);
  fiip.respond(cloud.txd, msg, errno, body);
};
const listener0x7829 = async (msg) => {
  const errno = 0x00;
  const body = Buffer.from([]);
  const id = Buffer.from(msg.body.slice(0, 8));
  const kLen = msg.body[8];
  const kVal = Buffer.from(msg.body.slice(9, 9 + kLen));
  const vLen = msg.body[9 + kLen];
  const vVal = Buffer.from(msg.body.slice(10 + kLen, 10 + kLen + vLen));
  cloud.vars.setTarget(id, kVal, kLen, vVal, vLen);
  fiip.respond(cloud.txd, msg, errno, body);
};

function start(self) {
  cloud = self;
  fiip.listener.add(0xffff, listener0xFFFF);
  fiip.listener.add(0xf011, listener0xF011);
  fiip.listener.add(0xf012, listener0xF012);
  fiip.listener.add(0xf013, listener0xF013);
  fiip.listener.add(0xf014, listener0xF014);
  fiip.listener.add(0xf015, listener0xF015);
  fiip.listener.add(0xf016, listener0xF016);
  fiip.listener.add(0x7811, listener0x7811);
  fiip.listener.add(0x7812, listener0x7812);
  fiip.listener.add(0xf021, listener0xF021);
  fiip.listener.add(0xf022, listener0xF022);
  fiip.listener.add(0xf023, listener0xF023);
  fiip.listener.add(0xf024, listener0xF024);
  fiip.listener.add(0xf025, listener0xF025);
  fiip.listener.add(0xf029, listener0xF029);
  fiip.listener.add(0xf02a, listener0xF02A);
  fiip.listener.add(0xf02b, listener0xF02B);
  fiip.listener.add(0xf02c, listener0xF02C);
  fiip.listener.add(0xf02d, listener0xF02D);
  fiip.listener.add(0x7821, listener0x7821);
  fiip.listener.add(0x7829, listener0x7829);
}
const Listeners = { start };
module.exports = Listeners;
